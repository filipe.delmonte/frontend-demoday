export interface Incident {
  incident_id: number;
  date: string;
  n_killed: number;
  n_injured: number;
  latitude: number;
  longitude: number;
  location: number;
  notes: string;
  categories: string
}