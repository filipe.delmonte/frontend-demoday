import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Incident } from './app.interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private http: HttpClient,
  ) {}

  public gundata(): Observable<Incident[]> {
    return this.http.get<Incident[]>('/api/gundata');
  }
}