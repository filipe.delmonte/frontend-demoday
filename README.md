# Gun violence visualisation tool

## Goal
Create a visualisation tool that enables the user to analyse gun violence on a map.

Your assesment has to match the following requirements:
1) Efficiently display the dataset exposed by the server on a map.
2) Filter data displayed on the map by category.

## Prerequisites
- NodeJS 10.x.x or higher

## Project structure
```
└──server     # express server
    ├── index.js
    ├── README.md
    ├── ...
    └── package.json
└──client     # angular project
    ├── e2e
    ├── src
    ├── README.md
    ├── tsconfig.json
    ├── ...
    ├── angular.json
    └── package.json
```
### Server
This is a simple express app that clusters our dataset by location using Geohash.
Feel free to change anything or even rewrite it in any language in order to fulfil your needs.

Run `$ npm install` to install it's dependencies and `$ npm start` to start the server on port 3000

### Client
The client should be built with Angular 8. Feel free to choose any library you want to help you with map/styling/etc...

Run `$ npm install` to install it's dependencies and `$ npm start` to serve your frontend on port 4200.

**Note** that we pre-configured the angular proxy `./client/proxy.conf.json` to proxy all request that start with `http://localhost:4200/api` to your server running on `http://localhost:3000` so you don't run into CORS problems and don't loose time setting up a local proxy.

## Evaluation process
Your project will be evaluated based on the following criterias:
- Your approach to solve the problem
- Communication
- Code quality
- UI/UX


## Extra  exercise
- Fetch datapoints distribution per category to provide insights to the user (about number of results) before he/she starts filtering.